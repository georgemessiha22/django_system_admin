################################################################################
## Celery helpers workers and beats treated togther
################################################################################
celery-start() {
cd $PROJECT_PATH &&
$PYTHON_VENV_PATH/bin/celery -A ora_backend.taskapp beat --scheduler=django_celery_beat.schedulers:DatabaseScheduler --logfile=$PROJECT_PATH/logs/celery/beat.log --loglevel="INFO"  --pidfile=$PROJECT_PATH/logs/celery/beat.pid --detach &&
$PYTHON_VENV_PATH/bin/celery worker -A ora_backend.taskapp --logfile=$PROJECT_PATH/logs/celery/worker.log --loglevel="INFO" --pidfile=$PROJECT_PATH/logs/celery/worker.pid --detach;
}

celery-kill () {
ps aux | grep 'celery worker' | awk '{print $2}' | xargs kill -9;
ps aux | grep 'beat' | awk '{print $2}' | xargs kill -9;
rm -f $PROJECT_PATH/logs/celery/*.pid;
}

alias celery-tail='tail -100f $PROJECT_PATH/logs/celery/worker.log'
alias celery-errors='tail -100f $PROJECT_PATH/logs/celery/worker.log | grep "ERROR"'
alias celery-stats="ps aux | grep 'celery worker'"
alias celery-worker-kill="ps aux | grep 'celery worker' | awk '{print \$2}' | xargs kill -9"


export CELERY_TAIL='tail -100f $PROJECT_PATH/logs/celery/worker.log'
export CELERY_TAIL_ERRORS='tail -10000f $PROJECT_PATH/logs/celery/worker.log | grep "ERROR"'
export CELERY_STATS="ps aux | grep 'celery worker'"
export CELERY_WORKER_KILL="ps aux | grep 'celery worker' | awk '{print \$2}' | xargs kill -9"

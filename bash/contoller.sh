install-project-dependencies () {
  sudo yum install -y gcc rh-python36 nano git mariadb-devel vim net-tools;
	httpd24-install;
  sudo yum install -y gcc-c++;
  sudo yum install -y unixODBC unixODBC-devel;
  sudo yum install -y cyrus-sasl.x86_64 cyrus-sasl-gssapi.x86_64 cyrus-sasl-plain.x86_64;
}

install-vim-plugins() {
  vim -c "PluginInstall";
}

ownerships-permissions () {
	mkdir -p $PROJECT_PATH/logs;
	mkdir -p $PROJECT_PATH/logs/celery/;
	sudo chown -R $USER:apache $PROJECT_PATH;
	sudo chmod -R 777 $PROJECT_PATH/logs;
	sudo setenforce 0;
}

copy-config-files () {
  sudo cp ~/gunicorn.conf /opt/rh/httpd24/root/etc/httpd/conf.d/gunicorn.conf
  sudo cp ~/.env $PROJECT_PATH/.env
}

project-init () {
	sudo mkdir -p $PROJECT_PATH && sudo chown -R $USER $PROJECT_PATH;
	sudo cp ~/aptplaton.si.francetelecom.fr_aptplaton_standard_x86_64_rhas70_RPMS.rhscl-1_.repo /etc/yum.repos.d;
  install-project-dependencies &&
  git-clone-project &&
  copy-config-files &&
	fetch-project &&
  rh-python-pip-install &&
	ownerships-permissions &&
	httpd24-copy-static &&
	django-database-migrate &&
	httpd24-restart &&
  celery-start
}

project-restart () {
	httpd24-kill &&
  celery-kill;
	copy-config-files &&
	fetch-project &&
  rh-python-pip-update &&
	ownerships-permissions &&
	httpd24-copy-static &&
	django-database-migrate &&
	httpd24-start &&
  celery-start
}

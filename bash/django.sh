################################################################################
## Django helpers
################################################################################
django-database-migrate() {
  for DATABASE in ${DATABASES}; do
    $PYTHON_VENV_PATH/bin/python $PROJECT_PATH/manage.py migrate --database=$DATABASE
  done
}

django-files-logs () {
  mkdir -p $PROJECT_PATH/logs &&
  mkdir -p $PROJECT_PATH/logs/celery/ &&
  sudo chmod 777 -R $PROJECT_PATH/logs &&
  sudo chown -R :apache /$PROJECT_PATH
}

alias django-shell-plus="$PYTHON_VENV_PATH/bin/python $PROJECT_PATH/manage.py shell_plus"

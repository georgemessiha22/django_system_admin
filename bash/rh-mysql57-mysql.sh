# control rh-mysql57-mysql

rh-mysql57-install() {
  sudo yum install -y rh-mysql57.x86_64 &&
  sudo systemctl enable rh-mysql57-mysqld &&
  sudo systemctl start  rh-mysql57-mysqld;
}

rh-mysql57-restart() {
sudo systemctl stop  rh-mysql57-mysqld;
sudo systemctl start  rh-mysql57-mysqld;
sudo systemctl status  rh-mysql57-mysqld;
}

# you will need to run this manual to configure mysql
# scl enable rh-mysql57 bash
# mysql_secure_installation
# mysql -u root -p
# CREATE USER 'database'@'%' IDENTIFIED BY 'database123';
# GRANT ALL PRIVILEGES ON * . * TO 'database'@'%';
# FLUSH PRIVILEGES;

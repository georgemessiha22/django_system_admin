# redis helpers

rh-redis32-restart(){
	sudo systemctl stop rh-redis32-redis &&
	sudo systemctl start rh-redis32-redis &&
	sudo systemctl status -l rh-redis32-redis;
}

rh-redis32-install(){
	sudo yum -y install rh-redis32-redis &&
	sudo systemctl enable rh-redis32-redis &&
	sudo systemctl start rh-redis32-redis;
}

#################################################
# rh-Python36 Pip helper methods
#################################################

rh-python36-install () {
sudo yum -y install rh-python36;
}

rh-python-pip-install () {
  sudo /opt/rh/rh-python36/root/bin/pip install --upgrade pip &&
  sudo /opt/rh/rh-python36/root/bin/pip install --upgrade virtualenv &&
  /opt/rh/rh-python36/root/bin/virtualenv $PYTHON_VENV_PATH &&
  $PYTHON_VENV_PATH/bin/pip install -r $PROJECT_PATH/requirements/production.txt
}

rh-python-pip-update () {
  sudo /opt/rh/rh-python36/root/bin/pip install --upgrade pip &&
  sudo /opt/rh/rh-python36/root/bin/pip install --upgrade virtualenv &&
  # if [[ ! -d $PYTHON_VENV_PATH ]] then; /opt/rh/rh-python36/root/bin/virtualenv $PYTHON_VENV_PATH; fi &&
  $PYTHON_VENV_PATH/bin/pip install -r $PROJECT_PATH/requirements/production.txt
}

################################################################################
## Apache (rh-httpd24-httpd) helper
################################################################################
httpd24-install () {
sudo yum install -y httpd24-httpd;
sudo yum install -y mod_wsgi rh-python36-mod_wsgi.x86_64 httpd24-mod_ssl.x86_64;
sudo systemctl enable httpd24-httpd;
}
httpd24-kill () {
	sudo systemctl stop httpd24-httpd
}
httpd24-start() {
	sudo systemctl start httpd24-httpd &&
	sudo systemctl status -l httpd24-httpd;
}
httpd24-restart() {
sudo systemctl stop httpd24-httpd &&
sudo systemctl start httpd24-httpd &&
sudo systemctl status -l httpd24-httpd;
}

httpd24-copy-static() {
STATIC_SERVE_PATH=/opt/rh/httpd24/root/var/www/html/
$PYTHON_VENV_PATH/bin/python $PROJECT_PATH/manage.py collectstatic -c --noinput &&
echo "Copying to apache started.." &&
sudo rm -r -f $STATIC_SERVE_PATH/* &&
sudo cp -r $PROJECT_PATH/static $STATIC_SERVE_PATH/ &&
sudo ls -a $STATIC_SERVE_PATH/ &&
echo "Copying to apache finished"
}

httpd24-own() {
  sudo chown -R :apache $PROJECT_PATH
}

alias httpd24-config="sudo nano /opt/rh/httpd24/root/etc/httpd/conf.d/gunicorn.conf"

alias httpd24-tail="sudo tail -100f /var/log/httpd24/access_log"
alias httpd24-tail-error="sudo tail -100f /var/log/httpd24/error_log"

alias httpd24-tail-gunicorn="sudo tail -100f /var/log/httpd24/gunicorn-access.log"
alias httpd24-tail-gunicorn-error="sudo tail -100f /var/log/httpd24/gunicorn-error.log"

export HTTPD24_TAIL="sudo tail -100f /var/log/httpd24/access_log"
export HTTPD24_TAIL_ERROR="sudo tail -100f /var/log/httpd24/error_log"
export HTTPD24_TAIL_GUNICORN="sudo tail -100f /var/log/httpd24/gunicorn-access.log"
export HTTPD24_TAIL_GUNICORN_ERROR="sudo tail -100f /var/log/httpd24/gunicorn-error.log"

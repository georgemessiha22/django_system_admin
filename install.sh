#! /bin/bash

project_path=$(dirname $(realpath $BASH_SOURCE))

echo "Script running at " $project_path

LINE="if [ -f $project_path/bash/.bashrc ]; then . $project_path/bash/.bashrc;fi"
LINE2="PROMPT_COMMAND='__setprompt'"
FILE="$HOME/.bashrc"


if ! [ -f ~/.bashrc ]; then
  touch ~/.bashrc;
fi

grep -qF -- "$LINE2" "$FILE" || grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
. ~/.bashrc;
install_bashrc_support;

import os
from typing import Sequence


ROOT_DIR_PATH = os.path.dirname(os.path.realpath(__file__))
BASH_DIR_PATH = os.path.join(ROOT_DIR_PATH, "bash")
BASH_FILES_PATHS = []
for (dirpath, dirnames, filenames) in os.walk(BASH_DIR_PATH):
    BASH_FILES_PATHS.extend(filenames)
    break

for i in range(len(BASH_FILES_PATHS)):
    BASH_FILES_PATHS[i] = os.path.join(BASH_DIR_PATH, BASH_FILES_PATHS[i])

BASHRC_FILE_PATH = os.path.join(ROOT_DIR_PATH, "env_files", ".ignore.bashrc")


def merge(
    output_file_path: str, merged_file_paths: Sequence[str], append_linesep: bool = True
) -> None:
    with open(output_file_path, "w") as output_file:
        for merged_file_path in merged_file_paths:
            if "ignore" not in merged_file_path:
                with open(merged_file_path, "r") as merged_file:
                    merged_file_content = merged_file.read()
                    output_file.write(merged_file_content)
                    if append_linesep:
                        output_file.write(os.linesep)


def main():
    merge(BASHRC_FILE_PATH, BASH_FILES_PATHS)


if __name__ == "__main__":
    main()

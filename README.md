# Django System admin

This project is aimed to build a bash support for Linux machines hosting
Django projects.

for example there is Apache configurations example for best practice,

The project now contains the following:
- python vim support
- more out of your terminal using bash file
- celery configurations and bash method to control it [To be]
- bash controllers for deployment


## Install

use the install.sh file to install this project in addition to make a VIRTUAL_ENV
and .env file for the project it also will create apache configurations if applicable

```bash
  . install.sh
```

## [LICENSE](./LICENSE)

## [CHANGELOG](./CHANGELOG)

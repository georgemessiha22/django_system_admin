# Server Setup Guideline

This document is created for generic deployment example of Django linux (Red Hat), MYSQL_DB and Apache.

Author: George Messiha (george.messiha@orange.com)

## Instructions

- change all of the document varibales to your requirments (< variable>).
- Please DO NOT COPY/PASTE any of the documents commands unless you know what you do.

## Dependinces

- install the following:

```bash
sudo yum install -y git nano gcc
```

## Configure project path

- edit in /etc/environment

```bash
PROJECT_PATH=/opt/application/ora/ORA_Backend
FRONTEND_PATH=$PROJECT_PATH/ora_backend/frontend
DJANGO_READ_DOT_ENV_FILE=TRUE
DJANGO_SETTINGS_MODULE=config.settings.production
PROJECT_BRANCH=master
```

- after config global variables logout and login again.

## Git

```bash
git clone <project link> $PROJECT_PATH
cd $PROJECT_PATH
git submodule update --init --recursive
git submodule update --recursive --remote
```

## Configure SCL repos on RH on machines

- create file /etc/yum.repos.d/aptplaton.si.francetelecom.fr*aptplaton_standard_x86_64_rhas70_RPMS.rhscl-1*.repo:

```bash
sudo nano /etc/yum.repos.d/aptplaton.si.francetelecom.fr_aptplaton_standard_x86_64_rhas70_RPMS.rhscl-1_.repo
```

- in /etc/yum.repos.d/aptplaton.si.francetelecom.fr*aptplaton_standard_x86_64_rhas70_RPMS.rhscl-1*.repo

```buildoutcfg
[aptplaton.si.francetelecom.fr_aptplaton_standard_x86_64_rhas70_RPMS.rhscl-1_]
name=added from: http://aptplaton.si.francetelecom.fr/aptplaton/standard/x86_64/rhas70/RPMS.rhscl-1/
baseurl=http://aptplaton.si.francetelecom.fr/aptplaton/standard/x86_64/rhas70/RPMS.rhscl-1/
enabled=1
gpgcheck=0
```

## Install mysqlclient on linux

```bash
sudo yum install -y mariadb-devel
```

## Install Python3.6

- use the following command:

```bash
sudo yum install -y rh-python36
```

## Install virtualenv and packages

- use the following:

```bash
sudo /opt/rh/rh-python36/root/bin/pip install virtualenv \
  && sudo /opt/rh/rh-python36/root/bin/pip install -r $PROJECT_PATH/requirements/production.txt \
  && /opt/rh/rh-python36/root/bin/virtualenv $PROJECT_PATH/venv \
  && $PROJECT_PATH/venv/bin/pip install -r $PROJECT_PATH/requirements/production.txt
```

## Install Apache

- install

```bash
sudo yum install -y mod_wsgi httpd24-httpd rh-python36-mod_wsgi.x86_64 && sudo yum install -y httpd24-mod_ssl.x86_64
```

- create conf file:

```bash
sudo nano /opt/rh/httpd24/root/etc/httpd/conf.d/gunicorn.conf
```

- configuration file /opt/rh/httpd24/root/etc/httpd/conf.d/<project_name>.conf

```conf
LoadModule ssl_module modules/mod_ssl.so

Listen <server_port>
<VirtualHost *:<server_port>>
        ServerAdmin <server_admin>

        ServerName <server_ip>
        ErrorLog /opt/rh/httpd24/root/etc/httpd/logs/gunicorn-error.log
        CustomLog /opt/rh/httpd24/root/etc/httpd/logs/gunicorn-access.log combined

        <Directory <project_path>>
          Require all granted
          Order Deny,Allow
          Allow from All
          Options Indexes FollowSymLinks MultiViews
          Satisfy Any

         <Files config/wsgi.py>
            Require all granted
            Order Deny,Allow
            Allow from All
            Options Indexes FollowSymLinks MultiViews
            Satisfy Any
          </Files>
        </Directory>

        WSGIDaemonProcess project_name python-path=<project_path>:<project_path>/venv/lib/python3.6/site-packages
        WSGIProcessGroup project_name
        WSGIScriptAlias / <project_path>/config/wsgi.py

        Alias /static "/opt/rh/httpd24/root/var/www/html/static"
        <Directory "/opt/rh/httpd24/root/var/www/html/static">
            Require all granted
            Order Deny,Allow
            Allow from All
            Options Indexes FollowSymLinks MultiViews
            Satisfy Any
        </Directory>

        SSLEngine on
        SSLCertificateFile <crt_file>
        SSLCertificateKeyFile <key_file>
</VirtualHost>
```

## .env file

- please create a file in the project folder, upon to setup requirements:

```bash
nano $PROJECT_PATH/.env
```

- Environment variables.

```txt
# General
# ------------------------------------------------------------------------------
# DJANGO_READ_DOT_ENV_FILE=True
DJANGO_SETTINGS_MODULE=config.settings.local
DJANGO_SECRET_KEY=asdafsd151gew1rf21c5dw2ef5as15casdSADSEG5543asdaf
DJANGO_ADMIN_URL=admin/
DJANGO_ALLOWED_HOSTS=<server ip>
DJANGO_DEBUG=False

# Security
# ------------------------------------------------------------------------------
# TIP: better off using DNS, however, redirect is OK too
DJANGO_SECURE_SSL_REDIRECT=False
DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS=True
DJANGO_SECURE_HSTS_PRELOAD=True
DJANGO_SECURE_CONTENT_TYPE_NOSNIFF=True

# Cookie
# ------------------------------------------------------------------------------
DJANGO_SESSION_COOKIE_SECURE=False
DJANGO_CSRF_COOKIE_SECURE=False
DJANGO_CSRF_COOKIE_HTTPONLY=False

# django-allauth
# ------------------------------------------------------------------------------
DJANGO_ACCOUNT_ALLOW_REGISTRATION=True

# Gunicorn
# ------------------------------------------------------------------------------
WEB_CONCURRENCY=4

# Database
# ------------------------------------------------------------------------------
DATABASE_AUTH_HOST=<Ip of the database>
DATABASE_AUTH_PORT=<port of the database>
DATABASE_AUTH_DB=<name of the database>
DATABASE_AUTH_USER=<user of the database>
DATABASE_AUTH_PASSWORD=<password of the database>
DATABASE_AUTH_ENGINE=django_prometheus.db.backends.mysql

DATABASE_ORA_HOST=<Ip of the database>
DATABASE_ORA_PORT=<port of the database>
DATABASE_ORA_DB=<name of the database>
DATABASE_ORA_USER=<user of the database>
DATABASE_ORA_PASSWORD=<password of the database>
DATABASE_ORA_ENGINE=django_prometheus.db.backends.mysql

DATABASE_BASF_HOST=<Ip of the database>
DATABASE_BASF_PORT=<port of the database>
DATABASE_BASF_DB=<name of the database>
DATABASE_BASF_USER=<user of the database>
DATABASE_BASF_PASSWORD=<password of the database>
DATABASE_BASF_ENGINE=django_prometheus.db.backends.mysql

# CONSTANTS ORANGE
# ------------------------------------------------------------------------------
REDIRECT_LINK_NAME=<URL in case of unauthorized user
```

## migrate database

- use the following

```bash
mkdir $PROJECT_PATH/logs
sudo chmod 777 $PROJECT_PATH/logs
$PROJECT_PATH/venv/bin/python $PROJECT_PATH/manage.py migrate
```

## change the permissions

- change folder permissions:

```bash
sudo chown -R :apache $PROJECT_PATH
sudo setenforce 0
```

### Install Redis

- install command

```bash
sudo yum install rh-redis32
```

- start redis

```bash
sudo systemctl enable rh-redis32-redis
sudo systemctl start rh-redis32-redis
sudo systemctl status rh-redis32-redis
```

- restart redis

```bash
sudo systemctl stop rh-redis32-redis
sudo systemctl start rh-redis32-redis
sudo systemctl status rh-redis32-redis
```

- config redis

```bash
sudo nano /etc/opt/rh/rh-redis32/redis.conf
```